package com.example.foodapp.fragments


import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.foodapp.HomeActivity
import com.example.foodapp.model.Post
import com.example.foodapp.R
import com.example.foodapp.utils.showToast
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.fragment_create_post.view.*
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class CreatePostFragment : Fragment() {

    private lateinit var auth: FirebaseAuth
    private lateinit var database: FirebaseDatabase
    private lateinit var storage: FirebaseStorage
    private lateinit var storageReference: StorageReference
    private lateinit var progressDialog: ProgressDialog
    private lateinit var description:String
    private lateinit var imgAdded: ImageView
    private var filePath: Uri? = null
    private lateinit var firebaseAnalytics: FirebaseAnalytics


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_create_post, container, false)
        // Inflate the layout for this fragment

        progressDialog = ProgressDialog(context)
        progressDialog.show()
        progressDialog.setCancelable(false)
        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
        storage = FirebaseStorage.getInstance()
        storageReference = storage.reference
        firebaseAnalytics = FirebaseAnalytics.getInstance(this.context!!)

        imgAdded = view.findViewById(R.id.img_added)


        view.btn_back.setOnClickListener {
            Intent(context,HomeActivity::class.java).apply{
                startActivity(this)
            }
        }

        view.btn_post.setOnClickListener{
            uploadImage()

        }

        CropImage.activity()
            .setAspectRatio(16,9)
            .start(view.context, this)

        return view

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE
            && resultCode == AppCompatActivity.RESULT_OK
            && data != null) {

            progressDialog.dismiss()

            // Get the Uri of data
            val result = CropImage.getActivityResult(data)
            filePath =  result.uri

            try {
                val bitmap = MediaStore.Images.Media.getBitmap(context?.contentResolver, filePath)
                imgAdded.setImageBitmap(bitmap)

            }catch (e: IOException){

                e.printStackTrace()
            }
        }else{

            Intent(context,HomeActivity::class.java).apply{
                startActivity(this)
            }
            progressDialog.dismiss()

        }
    }

    private fun uploadImage(){

        progressDialog.setTitle("uploading ...")
        progressDialog.show()


        val ref = storageReference.child("posts/" + UUID.randomUUID().toString())
        database = FirebaseDatabase.getInstance()

        if(filePath!=null){
            imgAdded.isDrawingCacheEnabled = true
            imgAdded.buildDrawingCache()
            val bitmap = (imgAdded.drawable as BitmapDrawable).bitmap
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data = baos.toByteArray()

            val uploadTask = ref.putBytes(data)
            uploadTask.continueWithTask { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                ref.downloadUrl
            }.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val downloadUri = task.result

                    val myUri = downloadUri.toString()
                    val postId = UUID.randomUUID().toString()
                    val currentTime = System.currentTimeMillis()
                    val user = auth.currentUser
                    description = view!!.ed_description.text.toString()

                    val post = Post(postId, user!!.uid, myUri, description, currentTime.toString())


                    val postRef = database.getReference("posts").child(postId)
                    postRef.setValue(post)
                        .addOnSuccessListener {
                            logEvent("Post Created", postId, user.uid)
                            context?.showToast("Post Added!")

                            FirebaseDatabase.getInstance().reference.child("follow")
                                .child(user.uid).child("following").child(user.uid).setValue(true)

//                            (context as FragmentActivity).supportFragmentManager.beginTransaction()
//                                .replace(R.id.fragment_container, HomeFragment())
//                                .commit()
                            Intent(context,HomeActivity::class.java).apply{
                                startActivity(this)
                            }
                        }
                        .addOnFailureListener {
                            context?.showToast("Failed.")
                        }
                    progressDialog.dismiss()



                }
            }
                .addOnFailureListener {
                    progressDialog.dismiss()
                    Toast.makeText(context, "Failed ", Toast.LENGTH_SHORT).show()
                }

        } //end filepath
        else{ context?.showToast("No Image Selected!")}

    }

    private fun logEvent(eventName: String, postId: String, postUserId: String) {
        val bundle = Bundle()
        bundle.putString("PostId", postId)
        bundle.putString("PostUserID", postUserId)
        firebaseAnalytics.logEvent(eventName, bundle)
    }

}

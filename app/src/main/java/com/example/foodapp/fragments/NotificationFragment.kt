package com.example.foodapp.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.foodapp.adapter.NotificationAdapter
import com.example.foodapp.model.Notification
import com.example.foodapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

/**
 * A simple [Fragment] subclass.
 */
class NotificationFragment : Fragment() {

    private var rvNotification: RecyclerView? = null
    private var adapter: NotificationAdapter? = null
    private lateinit var dataSource:MutableList<Notification>
    private var firebaseUser: FirebaseUser? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_notification, container, false)

        rvNotification = v.findViewById(R.id.rv_notification)

        dataSource = mutableListOf()
        adapter = context?.let { NotificationAdapter(it, dataSource) }
        adapter?.notifyDataSetChanged()
        rvNotification?.adapter = adapter
        readNotification()
        return v
    }

    private fun readNotification(){
        firebaseUser = FirebaseAuth.getInstance().currentUser
        val reference = FirebaseDatabase.getInstance().getReference("notifications")
            .child(firebaseUser!!.uid)

        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                dataSource.clear()
                for (snapshot: DataSnapshot in dataSnapshot.children){

                    val noti = snapshot.getValue(Notification::class.java)
                    dataSource.add(noti!!)
                }
                adapter?.notifyDataSetChanged()
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

    }

}

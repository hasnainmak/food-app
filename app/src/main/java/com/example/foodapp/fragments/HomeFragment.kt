package com.example.foodapp.fragments


import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.foodapp.adapter.PostAdapter
import com.example.foodapp.model.Post
import com.example.foodapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    private var rvAllPost: RecyclerView? = null
    private var adapter: PostAdapter? = null
    private lateinit var progressDialog: ProgressDialog
    private lateinit var dataSource:MutableList<Post>
    private lateinit var followingList: MutableList<String>
    private lateinit var txtNoPost: TextView



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_home, container, false)

        rvAllPost = v.findViewById(R.id.rv_allPost)
        progressDialog = ProgressDialog(context)
        progressDialog.setTitle("Loading ...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        txtNoPost = v.findViewById(R.id.txt_noPost)
        dataSource = mutableListOf()
        checkFollowing()


        return v
    }


    private fun checkFollowing(){
        followingList = mutableListOf()

        val reference = FirebaseDatabase.getInstance().getReference("follow")
            .child(FirebaseAuth.getInstance().currentUser!!.uid)
            .child("following")

        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                followingList.clear()
                for (snapshot: DataSnapshot in dataSnapshot.children){
                    snapshot.key?.let { followingList.add(it) }
                }
                readAllPost()
            }

            override fun onCancelled(databaseError: DatabaseError) {
        }
    })

}

    private fun readAllPost(){
        val reference = FirebaseDatabase.getInstance().getReference("posts")
            .orderByChild("currenTime")

        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                    dataSource.clear()
                    for (snapshot: DataSnapshot in dataSnapshot.children){
                        val post = snapshot.getValue(Post::class.java)
                        for(id in followingList){
                            if (post!!.userId == id) {
                                dataSource.add(post)
                            }
                        }

                        if(dataSource.size>0){
                            adapter = context?.let { PostAdapter(it, dataSource) }
                            adapter?.notifyDataSetChanged()
                            rvAllPost?.visibility = View.VISIBLE
                            rvAllPost?.adapter = adapter
                            txtNoPost.visibility = View.GONE
                        }else{
                            rvAllPost?.visibility = View.GONE
                            txtNoPost.visibility = View.VISIBLE
                        }
                    }
                dataSource.reverse()
                adapter?.notifyDataSetChanged()
                progressDialog.dismiss()
                }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

    }


}

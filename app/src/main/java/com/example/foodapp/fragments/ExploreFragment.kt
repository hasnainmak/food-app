package com.example.foodapp.fragments


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.example.foodapp.adapter.UserAdapter
import com.example.foodapp.model.User
import com.example.foodapp.R
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

/**
 * A simple [Fragment] subclass.
 */
class ExploreFragment : Fragment() {


    private var rvSearch: RecyclerView? = null
    private var adapter: UserAdapter? = null
    private lateinit var dataSource:MutableList<User>
    private lateinit var search:EditText
    private lateinit var progressBar:ProgressBar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_explore, container, false)

        rvSearch = v.findViewById(R.id.rv_allPost)
        search = v.findViewById(R.id.ed_search)
        progressBar = v.findViewById((R.id.progressBar))

        dataSource = mutableListOf()
        adapter = context?.let { UserAdapter(it, dataSource) }
        adapter?.notifyDataSetChanged()
        rvSearch?.adapter = adapter
        readUser()




        search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                progressBar.visibility = View.VISIBLE

                searchQuery(s.toString())


            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        return v
    }

    private fun searchQuery(s:String){
        val query = FirebaseDatabase.getInstance().getReference("users")
            .orderByChild("name")
            .startAt(s)
            .endAt(s+"\uf8ff")

        query.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                dataSource.clear()
                for (snapshot:DataSnapshot in dataSnapshot.children){
                    val userSearch = snapshot.getValue(User::class.java)
                    dataSource.add(userSearch!!)

                }
                adapter?.notifyDataSetChanged()
                progressBar.visibility = View.GONE

            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
    }


    private fun readUser(){
        val reference = FirebaseDatabase.getInstance().getReference("users")

        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                if (search.text.toString() == "") {
                    dataSource.clear()
                    for (snapshot: DataSnapshot in dataSnapshot.children){
                        val user = snapshot.getValue(User::class.java)
                        dataSource.add(user!!)

                    }
                }
                adapter?.notifyDataSetChanged()

            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

    }

}

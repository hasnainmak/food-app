package com.example.foodapp.fragments


import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.Intent.*
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.example.foodapp.EditProfileActivity
import com.example.foodapp.LoginActivity
import com.example.foodapp.model.Notification
import com.example.foodapp.model.User
import com.example.foodapp.MyPagerFragment
import com.example.foodapp.R
import com.example.foodapp.utils.Constant
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*


class ProfileFragment : Fragment() {


    private lateinit var database: FirebaseDatabase
    private var firebaseUser: FirebaseUser? = null
    private lateinit var progressDialog: ProgressDialog
    private lateinit var tabLayout:TabLayout
    private lateinit var viewPager:ViewPager
    private lateinit var preferences: SharedPreferences
    private lateinit var btn_editProfile:Button
    private lateinit var txt_following:TextView
    private lateinit var txt_followers:TextView
    private var profileId:String? = null
    private lateinit var imgView: CircleImageView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_profile, container, false)

        progressDialog = ProgressDialog(context)
        progressDialog.setTitle("Loading...")
        progressDialog.setCancelable(false)
     //   progressBar = view.findViewById(R.id.progressBar)
        preferences = context!!.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE)
        profileId = preferences.getString(Constant.KEY_PROFILE_ID,"")
        database = FirebaseDatabase.getInstance()

        fetchData()

        tabLayout = view.findViewById(R.id.tabLayout)
        imgView = view.findViewById(R.id.imgView)
        viewPager = view.findViewById(R.id.viewPager)
        txt_followers = view.findViewById(R.id.txt_follower)
        txt_following = view.findViewById(R.id.txt_following)
        btn_editProfile = view.findViewById(R.id.btn_editProfile)


        val fragmentAdapter = MyPagerFragment(view.context,childFragmentManager)
        viewPager.adapter = fragmentAdapter

        tabLayout.setupWithViewPager(viewPager)
        tabLayout.getTabAt(0)?.setIcon(R.drawable.ic_grid_24dp)
        tabLayout.getTabAt(1)?.setIcon(R.drawable.ic_restaurant_menu_black_24dp)
        tabLayout.getTabAt(2)?.setIcon(R.drawable.ic_assignment_black_24dp)



        firebaseUser = FirebaseAuth.getInstance().currentUser
        fetchData()
        getFollowers()



        if (profileId.toString() == firebaseUser?.uid) {
            btn_editProfile.text = getString(R.string.edit_profile)
            view.btn_logOut.visibility = View.VISIBLE
            view.btn_logOut.setOnClickListener {

                FirebaseAuth.getInstance().signOut()

                val intent = Intent(context, LoginActivity::class.java)
                intent.flags = FLAG_ACTIVITY_CLEAR_TOP or FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)

            }
        }else{
            view.btn_logOut.visibility = View.GONE
            checkFollow()
        }

        btn_editProfile.setOnClickListener {

            when {
                btn_editProfile.text.toString() == "Edit" -> Intent(
                    activity,
                    EditProfileActivity::class.java
                ).apply {
                    startActivity(this)
                }
                btn_editProfile.text.toString() == "Follow" -> {
                    FirebaseDatabase.getInstance().reference.child("follow")
                        .child(firebaseUser!!.uid).child("following").child(profileId.toString())
                        .setValue(true)
                    FirebaseDatabase.getInstance().reference.child("follow")
                        .child(profileId.toString()).child("followers").child(firebaseUser!!.uid)
                        .setValue(true)
                    addNotifications()
                }
                btn_editProfile.text.toString() == "Following" -> {
                    FirebaseDatabase.getInstance().reference.child("follow")
                        .child(firebaseUser!!.uid).child("following").child(profileId.toString())
                        .removeValue()
                    FirebaseDatabase.getInstance().reference.child("follow")
                        .child(profileId.toString()).child("followers").child(firebaseUser!!.uid)
                        .removeValue()

                    FirebaseDatabase.getInstance().reference.child("notifications")
                        .child(profileId!!)
                        .child("isFollowing").removeValue()
                }
            }

        }

        return view
    }


    private fun checkFollow(){
        val database = FirebaseDatabase.getInstance().reference
            .child("follow").child(firebaseUser!!.uid).child("following")

        database.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if(dataSnapshot.child(profileId.toString()).exists()){
                    btn_editProfile.text = "Following"
                }else{
                    btn_editProfile.text = "Follow"
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
    }

    private fun addNotifications(){
        val ref = FirebaseDatabase.getInstance().reference
            .child("notifications").child(profileId.toString())

        val addNotification = Notification(
            "","is following you",firebaseUser!!.uid,""
        )

        ref.child("isFollowing").setValue(addNotification)

    }


    private fun getFollowers(){
        val database = FirebaseDatabase.getInstance().reference
            .child("follow").child(profileId.toString()).child("followers")

        database.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                txt_followers.text = "${dataSnapshot.childrenCount}"
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

        val database1 = FirebaseDatabase.getInstance().reference
            .child("follow").child(profileId.toString()).child("following")

        database1.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                txt_following.text = "${dataSnapshot.childrenCount}"
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

    }


    private fun fetchData(){
     //   progressBar.visibility = View.VISIBLE
       progressDialog.show()
        val reference = FirebaseDatabase.getInstance().getReference("users")
            .child(profileId.toString())

        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                if(context==null){
                    return
                }

                    val user = dataSnapshot.getValue(User::class.java)
                    Glide.with(view!!.context).load(user!!.profileImg).into(imgView)
                txt_name.text = user.name

               //     progressBar.visibility = View.GONE
             progressDialog.dismiss()

            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

    }




}

package com.example.foodapp

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.foodapp.utils.showToast
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T



class LoginActivity : AppCompatActivity(){

    private lateinit var email: String
    private lateinit var password: String
    private lateinit var progressDialog: ProgressDialog
    private lateinit var auth: FirebaseAuth
    private lateinit var firebaseAnalytics: FirebaseAnalytics


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        progressDialog = ProgressDialog(this)
        auth = FirebaseAuth.getInstance()
        progressDialog.setTitle("Please wait..")
        progressDialog.setCancelable(false)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        btn_login.setOnClickListener {
            email = ed_login_email.text.toString().trim()
            password = ed_login_password.text.toString()


            if (email.isEmpty() || password.isEmpty()) {
                showToast("fields are required")

            }else{
                progressDialog.show()
                auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            logEvent("User Login")
                            // Sign in success, update UI with the signed-in user's information
                            Intent(this, HomeActivity::class.java).apply {
                                startActivity(this)
                                finish()
                                progressDialog.dismiss()
                            }


                        } else {
                            // If sign in fails, display a message to the user.
                            progressDialog.dismiss()
                            Toast.makeText(baseContext, "${task.exception!!.message}",
                                Toast.LENGTH_SHORT).show()

                            // ...
                        }

                        // ...
                    }


            }
        }



        txt_login_register.setOnClickListener {
            Intent(this, RegisterActivity::class.java).apply {
                startActivity(this)
                finish()
            }

        }
    }

    override fun onStart() {
        super.onStart()
        if(auth.currentUser!=null){
            Intent(this, HomeActivity::class.java).apply {
                startActivity(this)
                finish()
            }

        }
    }


    private fun logEvent(eventName: String) {
        val bundle = Bundle()
        bundle.putString("Email", email)
        firebaseAnalytics.logEvent(eventName, bundle)
    }

}

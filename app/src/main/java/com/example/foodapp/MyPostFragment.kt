package com.example.foodapp


import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.foodapp.adapter.MyPhotoAdapter
import com.example.foodapp.model.Post
import com.example.foodapp.utils.Constant
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

/**
 * A simple [Fragment] subclass.
 */
class MyPostFragment : Fragment() {

    private var rvMyPost: RecyclerView? = null
    private var adapter: MyPhotoAdapter? = null
    private lateinit var progressDialog: ProgressDialog
    private lateinit var dataSource:MutableList<Post>
    private lateinit var preferences: SharedPreferences
    private var profileId:String? = null
    private lateinit var txt_noPost:TextView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_my_post, container, false)

        rvMyPost = view.findViewById(R.id.rv_allPost)
        progressDialog = ProgressDialog(context)
        progressDialog.setTitle("Loading ...")
        progressDialog.setCancelable(false)
        preferences = context!!.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE)
        profileId = preferences.getString(Constant.KEY_PROFILE_ID,"")
        txt_noPost = view.findViewById(R.id.txt_noPost)
        dataSource = mutableListOf()
        readAllPost()

        return view
    }

    private fun readAllPost(){
        progressDialog.show()
        val reference = FirebaseDatabase.getInstance().getReference("posts")
            .orderByChild("currenTime")

        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                dataSource.clear()
                for (snapshot: DataSnapshot in dataSnapshot.children){
                    val post = snapshot.getValue(Post::class.java)
                    if (post!!.userId == profileId) {
                            dataSource.add(post)
                        }
                    dataSource.reverse()
                    adapter?.notifyDataSetChanged()

                    }

                    if(dataSource.size>0){
                        adapter = context?.let { MyPhotoAdapter(it, dataSource) }
                        adapter?.notifyDataSetChanged()
                        rvMyPost?.visibility = View.VISIBLE
                        rvMyPost?.adapter = adapter
                        txt_noPost.visibility = View.GONE
                    }else{
                        rvMyPost?.visibility = View.GONE
                        txt_noPost.visibility = View.VISIBLE
                    }
                progressDialog.dismiss()
            }



            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

    }

}

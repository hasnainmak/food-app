package com.example.foodapp


import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.foodapp.adapter.ReservationAdapter
import com.example.foodapp.model.Reservation
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


/**
 * A simple [Fragment] subclass.
 */
class ShowReservation : Fragment() {

    private var firebaseUser: FirebaseUser? = null
    private lateinit var progressDialog: ProgressDialog
    private var rvMyReservation: RecyclerView? = null
    private var adapter: ReservationAdapter? = null
    private lateinit var dataSource:MutableList<Reservation>
    private lateinit var txtNoPost: TextView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_show_reservation, container, false)

        progressDialog = ProgressDialog(context)
        progressDialog.setTitle("Loading...")
        progressDialog.setCancelable(false)
        rvMyReservation = v.findViewById(R.id.rv_allReservation)
        txtNoPost = v.findViewById(R.id.txt_noPost)

        dataSource = mutableListOf()

        readReservations()
        return v
    }


    private fun readReservations(){
        progressDialog.show()
        firebaseUser = FirebaseAuth.getInstance().currentUser
        val reference = FirebaseDatabase.getInstance().getReference("reservations")

        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                dataSource.clear()
                for (snapshot: DataSnapshot in dataSnapshot.children){
                    val reserve = snapshot.getValue(Reservation::class.java)
                    if (reserve!!.bookTo == firebaseUser!!.uid || reserve.userId == firebaseUser!!.uid) {
                        dataSource.add(reserve)
                    }

                    adapter?.notifyDataSetChanged()

                }

                if(dataSource.size>0){
                    adapter = context?.let { ReservationAdapter(it, dataSource) }
                    adapter?.notifyDataSetChanged()
                    rvMyReservation?.visibility = View.VISIBLE
                    rvMyReservation?.adapter = adapter
                    txtNoPost.visibility = View.GONE
                }else{
                    rvMyReservation?.visibility = View.GONE
                    txtNoPost.visibility = View.VISIBLE
                }
                progressDialog.dismiss()
            }



            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

    }



}

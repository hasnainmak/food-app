package com.example.foodapp


import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.foodapp.model.User
import com.example.foodapp.utils.Constant
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
/**
 * A simple [Fragment] subclass.
 */
class ProfileDetailsFragment : Fragment() {

    private lateinit var database: FirebaseDatabase
    private lateinit var progressDialog: ProgressDialog
    private var firebaseUser: FirebaseUser? = null
    private var profileId:String? = null
    private lateinit var preferences: SharedPreferences
    private lateinit var txt_address:TextView
    private lateinit var txt_phone:TextView
    private lateinit var txt_timming:TextView
    private lateinit var contactInfo:TextView
    private lateinit var img_phone:ImageView
    private lateinit var image_location:ImageView
    private lateinit var icon_clock:ImageView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view=  inflater.inflate(R.layout.fragment_profile_details, container, false)

        txt_address = view.findViewById(R.id.txt_address)
        txt_phone = view.findViewById(R.id.txt_phone)
        txt_timming = view.findViewById(R.id.txt_timing)
        contactInfo = view.findViewById(R.id.contactInfo)
        image_location = view.findViewById(R.id.image_location)
        icon_clock = view.findViewById(R.id.icon_clock)
        img_phone = view.findViewById(R.id.img_phone)



        progressDialog = ProgressDialog(context)
        preferences = context!!.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE)
        profileId = preferences.getString(Constant.KEY_PROFILE_ID,"")
        firebaseUser = FirebaseAuth.getInstance().currentUser
        database = FirebaseDatabase.getInstance()


        if (profileId.toString() == firebaseUser?.uid) {
            fetchMyData()
        }else{
            fetchOtherData()
        }


        return view
    }


    private fun fetchMyData(){
        progressDialog.show()


        val reference = FirebaseDatabase.getInstance().getReference("users")
            .child(firebaseUser!!.uid)
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                val user = dataSnapshot.getValue(User::class.java)

                if(user?.address.equals("") &&
                    user?.phone.equals("") &&
                    user?.shopTiming.equals("")){

                    contactInfo.text = "No information is provided by this user."
                    contactInfo.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL)
                    contactInfo.gravity = Gravity.CENTER_VERTICAL
                }


                if(user?.phone.equals("")){
                    txt_phone.visibility = View.GONE
                    img_phone.visibility  = View.GONE

                }else{
                    txt_phone.text = user?.phone
                }

                if(user?.address.equals("")){
                    txt_address.visibility = View.GONE
                    image_location.visibility  = View.GONE

                }else {
                    txt_address.text = user?.address
                }


                if(user?.shopTiming.equals("")){
                    txt_timming.visibility = View.GONE
                    icon_clock.visibility  = View.GONE
                }else {
                    txt_timming.text = user?.shopTiming
                }

                progressDialog.dismiss()

            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

    }

    private fun fetchOtherData(){
        progressDialog.show()


        val reference = FirebaseDatabase.getInstance().getReference("users")
            .child(profileId.toString())
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                val user = dataSnapshot.getValue(User::class.java)

                if(user?.address.equals("") &&
                    user?.phone.equals("") &&
                    user?.shopTiming.equals("")){

                    contactInfo.text = "No information is provided by this user."
                    contactInfo.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL)
                    contactInfo.gravity = Gravity.CENTER_VERTICAL
                }


                if(user?.phone.equals("")){
                    txt_phone.visibility = View.GONE
                    img_phone.visibility  = View.GONE

                }else{
                    txt_phone.text = user?.phone
                }

                if(user?.address.equals("")){
                    txt_address.visibility = View.GONE
                    image_location.visibility  = View.GONE

                }else {
                    txt_address.text = user?.address
                }


                if(user?.shopTiming.equals("")){
                    txt_timming.visibility = View.GONE
                    icon_clock.visibility  = View.GONE
                }else {
                    txt_timming.text = user?.shopTiming
                }




                progressDialog.dismiss()

            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

    }


}

package com.example.foodapp


import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.foodapp.adapter.PostAdapter
import com.example.foodapp.model.Post
import com.example.foodapp.utils.Constant
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_post_detail.view.*
import com.example.foodapp.fragments.ProfileFragment


/**
 * A simple [Fragment] subclass.
 */
class PostDetailFragment : Fragment() {

    private var rvAllPost: RecyclerView? = null
    private var adapter: PostAdapter? = null
    private lateinit var progressDialog: ProgressDialog
    private lateinit var dataSource:MutableList<Post>
    private lateinit var preferences: SharedPreferences
    private var profileId:String? = null
    private var postId:String? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_post_detail, container, false)

        rvAllPost = v.findViewById(R.id.rv_my_allPost)
        preferences = context!!.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE)
        profileId = preferences.getString(Constant.KEY_PROFILE_ID,"")
        progressDialog = ProgressDialog(context)
        progressDialog.setTitle("Loading ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        v.btn_back.setOnClickListener {
            (context as FragmentActivity).supportFragmentManager.popBackStack()

        }

        dataSource = mutableListOf()
//        rvAllPost?.findViewHolderForLayoutPosition(holderPosition)!!.itemView
        postId = preferences.getString(Constant.KEY_POST_ID,"")

        val reference = FirebaseDatabase.getInstance().getReference("posts")
            .child(postId!!)

        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                dataSource.clear()


                if(dataSnapshot.exists()){
                    val post = dataSnapshot.getValue(Post::class.java)
                    dataSource.add(post!!)
                    adapter?.notifyDataSetChanged()

                }else{
                    (context as FragmentActivity).supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, ProfileFragment())
                        .commit()
                }

                progressDialog.dismiss()
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

        adapter = context?.let { PostAdapter(it, dataSource) }
        rvAllPost?.adapter = adapter


        return v
    }



}

package com.example.foodapp

import android.content.Context
import android.content.SharedPreferences
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.foodapp.utils.Constant
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser


class MyPagerFragment(var context: Context,fm:FragmentManager):FragmentPagerAdapter(fm) {

    private var firebaseUser: FirebaseUser? = null
    private lateinit var sharedPreferences: SharedPreferences
    private var profileId:String? = null

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                MyPostFragment()
            }
            1 ->{
                firebaseUser = FirebaseAuth.getInstance().currentUser
                sharedPreferences = context.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE)
                profileId = sharedPreferences.getString(Constant.KEY_PROFILE_ID,"")
                if(firebaseUser!!.uid == profileId){
                    ShowReservation()
                }else{
                 ReservationFragment()
                }

            }
            else -> {
                return ProfileDetailsFragment()
            }
        }
    }



    override fun getCount(): Int {
        return 3
        }



}
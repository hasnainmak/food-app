package com.example.foodapp

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.foodapp.model.User
import com.example.foodapp.utils.showToast
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {


    private lateinit var name:String
    private lateinit var email:String
    private lateinit var password:String
    private lateinit var auth: FirebaseAuth
    private lateinit var progressDialog:ProgressDialog
    private lateinit var database:FirebaseDatabase
    private lateinit var firebaseAnalytics: FirebaseAnalytics


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        firebaseAnalytics = FirebaseAnalytics.getInstance(this)



        txt_register_login.setOnClickListener {
            Intent(this, LoginActivity::class.java).apply {
                startActivity(this)
                finish()
            }
        }


         progressDialog = ProgressDialog(this)
        auth = FirebaseAuth.getInstance()

//        radioGroup.setOnCheckedChangeListener { group, checkedId ->
//            if (checkedId == R.id.radioUser){
//                account_type = radioUser.text.toString()
//                }
//            if (checkedId == R.id.radioBusiness){
//                account_type = radioBusiness.text.toString()
//            }
//        }

        progressDialog.setTitle("Please wait..")
        progressDialog.setCancelable(false)

        btn_register.setOnClickListener {

            name = ed_name.text.toString().trim()
            email = ed_email.text.toString().trim()
            password = ed_password.text.toString().trim()



            if (name.isEmpty()
                || email.isEmpty()
                || password.isEmpty()
            ) {
                showToast("All fields are required")
            }  else if(password.length<6)
                    {
                        showToast("Please enter password more than 6 digits")
                    }
            else{
                progressDialog.show()
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            logEvent("Registered User")
                            database = FirebaseDatabase.getInstance()
                            val user = auth.currentUser
                            val registerDetails = User(
                                user!!.uid,
                                name,
                                email,
                                password,
                                "",
                                "",
                                "",
                                "https://firebasestorage.googleapis.com/v0/b/foodapp-21aa7.appspot.com/o/images%2Fsocial.png?alt=media&token=b902102f-c774-4c90-b52f-19e9d526a0f8"
                            )
                            val myRef = database.getReference("users").child(user.uid)
                            myRef.setValue(registerDetails)
                            progressDialog.dismiss()
                            Intent(this, HomeActivity::class.java).apply {
                                startActivity(this)
                                finish()
                            }


                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(baseContext, "${task.exception!!.message}",
                                Toast.LENGTH_SHORT).show()


                        }

                        // ...
                    }


            }
        }


    }

    override fun onStart() {
        super.onStart()
        if(auth.currentUser!=null){
            Intent(this, HomeActivity::class.java).apply {
                startActivity(this)
                finish()
            }

        }
    }

    private fun logEvent(eventName: String) {
        val bundle = Bundle()
        bundle.putString("Name", name)
        firebaseAnalytics.logEvent(eventName, bundle)
    }

}

package com.example.foodapp


import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import com.example.foodapp.fragments.ProfileFragment
import com.example.foodapp.model.Reservation
import com.example.foodapp.utils.Constant
import com.example.foodapp.utils.showToast
import com.google.firebase.analytics.FirebaseAnalytics
import com.jaredrummler.materialspinner.MaterialSpinner
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.fragment_reservation.*
import kotlinx.android.synthetic.main.fragment_reservation.view.*
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class ReservationFragment : Fragment() {

    private lateinit var spinner:MaterialSpinner
    private var date:String? = null
    private var time:String? = null
    private lateinit var auth: FirebaseAuth
    private lateinit var database: FirebaseDatabase
    private lateinit var noOfPerson:String
    private lateinit var progressDialog: ProgressDialog
    private lateinit var preferences: SharedPreferences
    private var profileId:String? = ""
    private lateinit var firebaseAnalytics: FirebaseAnalytics


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_reservation, container, false)

        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
        firebaseAnalytics = FirebaseAnalytics.getInstance(this.context!!)
        preferences = context!!.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE)
        profileId = preferences.getString(Constant.KEY_PROFILE_ID,"")
        progressDialog = ProgressDialog(context)
        progressDialog.setTitle("Please wait ...")


        spinner = v.findViewById(R.id.spinner)
        spinner.setItems("1 Person", "2 Person", "3 Person", "4 Person", "5 Person","6 Person", "7 Person", "8 Person", "9 Person", "10 Person")
        spinner.setOnItemSelectedListener { _, _, _, item ->
            noOfPerson = item.toString()
        }

        v.btn_book.setOnClickListener {
            val name = v.ed_name.text.toString()
            val phone = v.ed_phone.text.toString()
            val description = v.ed_description.text.toString()

             val regexStr = arrayOf(1,2,3,4,5,6,7,8,9,10,11)
             val save= regexStr.size


            if (name.isEmpty() ||
                    phone.isEmpty() ||
                    date.isNullOrEmpty() ||
                    time.isNullOrEmpty() ||
                    noOfPerson.isEmpty())
            {
                context!!.showToast("Fields are required.")
            }
            else if(phone.length!=save)
            {
                context!!.showToast("please enter correct phone number")
            }
            else {
                progressDialog.show()

                val user = auth.currentUser
                database = FirebaseDatabase.getInstance()


                val reservationID = UUID.randomUUID().toString()
                val userRef = database.getReference("reservations").child(reservationID)


                val reserve = Reservation(
                    reservationID,
                    user!!.uid,
                    profileId.toString(),
                    name,
                    phone,
                    noOfPerson,
                    date,
                    time,
                    "false",
                    description
                )
                userRef.setValue(reserve)
                logEvent("Reservation Created", reservationID, name)
                context?.showToast("Reservation placed!")
                progressDialog.dismiss()

                (context as FragmentActivity).supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, ProfileFragment())
                    .commit()
            }


        }

        v.btn_getDate.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)


            val dpd = DatePickerDialog(
                view!!.context,
                DatePickerDialog.OnDateSetListener { _, year, _, _ ->

                date = "$day/$month/$year"
                    btn_getDate.text = "$date"

            }, year, month, day)

            dpd.show()

        }

        v.btn_getTime.setOnClickListener {
            val cal = Calendar.getInstance()
            val hour = cal.get(Calendar.HOUR)
            val min = cal.get(Calendar.MINUTE)


            val timeSetListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)

                time = SimpleDateFormat("HH:mm").format(cal.time)
                btn_getTime.text = "$time"
            }
            TimePickerDialog(context, timeSetListener, hour, min, false).show()
        }

        return v
    }


    private fun logEvent(eventName: String, bookId: String, bookerName: String) {
        val bundle = Bundle()
        bundle.putString("Reservation ID", bookId)
        bundle.putString("Booker Name", bookerName)
        firebaseAnalytics.logEvent(eventName, bundle)
    }



}

package com.example.foodapp

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.foodapp.model.User
import com.example.foodapp.utils.showToast
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_edit_profile.imgView
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*

class EditProfileActivity : AppCompatActivity() {

    private lateinit var database: FirebaseDatabase
    private lateinit var storage: FirebaseStorage
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var storageReference: StorageReference
    private lateinit var progressDialog: ProgressDialog
    private lateinit var name:String
    private var address:String? = ""
    private var shopTiming:String? = ""
    private var phone:String? = ""
    private var firebaseUser: FirebaseUser? = null
    private var filePath: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        progressDialog = ProgressDialog(this)
        firebaseUser = FirebaseAuth.getInstance().currentUser
        database = FirebaseDatabase.getInstance()
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        storage = FirebaseStorage.getInstance()
        storageReference = storage.reference

        btn_selectImg.setOnClickListener {
            selectImage()
        }

        btn_save.setOnClickListener {
            update()
            progressDialog.dismiss()

        }
        btn_back.setOnClickListener {
            finish()
        }

        fetchData()


    }


    private fun uploadImage(){

            progressDialog.setTitle("uploading ...")
            progressDialog.show()


            val ref = storageReference.child("profile/" + UUID.randomUUID().toString())
            database = FirebaseDatabase.getInstance()

        if(filePath!=null){
            imgView.isDrawingCacheEnabled = true
            imgView.buildDrawingCache()
            val bitmap = (imgView.drawable as BitmapDrawable).bitmap
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data = baos.toByteArray()


            val uploadTask = ref.putBytes(data)
            uploadTask.continueWithTask { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                ref.downloadUrl
            }.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val downloadUri = task.result
                    val myUri = downloadUri.toString()

                    val userRef = database.getReference("users").child(firebaseUser!!.uid)
                    userRef.child("profileImg")
                        .setValue(myUri)
                        .addOnSuccessListener {
                            showToast("Profile Pic Updated!")
                        }
                        .addOnFailureListener {
                            showToast("Failed to update!")
                        }
                    progressDialog.dismiss()


                }
            }
                .addOnFailureListener {
                    progressDialog.dismiss()
                    Toast.makeText(this, "Failed ", Toast.LENGTH_SHORT).show()                }

        }//end filepath

        }

    private fun update(){

        progressDialog.show()
        name = ed_name.text.toString()
        address = ed_address.text.toString().toUpperCase(Locale.getDefault())
        phone = ed_phone.text.toString()
        shopTiming = ed_timing.text.toString().toUpperCase(Locale.getDefault())

        database = FirebaseDatabase.getInstance()
        when {
            name.isEmpty() -> showToast("Name is required.")
            phone!!.length>=12 -> showToast("Required less than 11 digits")
            else -> {
                val userRef = database.getReference("users").child(firebaseUser!!.uid)
                userRef.child("name").setValue(name)
                userRef.child("address").setValue(address)
                userRef.child("phone").setValue(phone)
                userRef.child("shopTiming").setValue(shopTiming)

                logEvent(name)
                showToast("Profile Updated")
                finish()
            }
        }
    }


    private fun selectImage(){


        CropImage.activity()
            .setAspectRatio(1,1)
            .setCropShape(CropImageView.CropShape.OVAL)
            .start(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE
            && resultCode == RESULT_OK
            && data != null) {


            // Get the Uri of data
            val result = CropImage.getActivityResult(data)
            filePath =  result.uri

            try {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
                imgView.setImageBitmap(bitmap)
                uploadImage()

            }catch (e: IOException){

                e.printStackTrace()
            }
        }
    }


    private fun fetchData(){
        progressDialog.show()
        val reference = FirebaseDatabase.getInstance().getReference("users")
            .child(firebaseUser!!.uid)

        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val user = dataSnapshot.getValue(User::class.java)
                    Glide.with(applicationContext).load(user!!.profileImg).into(imgView)
                    ed_name.setText(user.name)
                    ed_phone.setText(user.phone)
                    ed_address.setText(user.address)
                    ed_timing.setText(user.shopTiming)
                progressDialog.dismiss()
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

    }

    private fun logEvent(username: String) {
        val bundle = Bundle()
        bundle.putString("Username", username)
        firebaseAnalytics.logEvent("Edit_Profile", bundle)
    }

}

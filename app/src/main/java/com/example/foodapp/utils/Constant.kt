package com.example.foodapp.utils

object Constant {

    const val PREF_NAME = "pref_food_app"

    const val KEY_PROFILE_ID = "profileId"
    const val KEY_POST_ID = "postId"
    const val KEY_RESERVATION_PROFILE= "reservationID"

}
package com.example.foodapp.adapter

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.foodapp.model.User
import com.example.foodapp.R
import com.example.foodapp.utils.Constant
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.user_item.view.*
import androidx.fragment.app.FragmentActivity
import com.example.foodapp.fragments.ProfileFragment
import com.example.foodapp.model.Notification
import com.google.firebase.analytics.FirebaseAnalytics
import de.hdodenhof.circleimageview.CircleImageView


class UserAdapter(var context: Context, var dataSource: MutableList<User>) :
    RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    private var firebaseUser: FirebaseUser? = null
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.user_item, parent, false)
        return UserViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataSource.size
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        firebaseUser = FirebaseAuth.getInstance().currentUser
        firebaseAnalytics = FirebaseAnalytics.getInstance(context)

        val user = dataSource[position]
        holder.follow.visibility = View.VISIBLE
        holder.name.text = user.name
        Glide.with(context).load(user.profileImg).into(holder.img)

        isFollowing(user.id,holder.follow)

        if (user.id == firebaseUser?.uid) {
            holder.follow.visibility = View.GONE
        }
        holder.itemView.setOnClickListener {
            sharedPreferences = context.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(Constant.KEY_PROFILE_ID,user.id)
            editor.apply()

            (context as FragmentActivity).supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, ProfileFragment())
                .commit()

        }


        holder.follow.setOnClickListener {
            if (holder.follow.text.toString() == "Follow") {
                FirebaseDatabase.getInstance().reference.child("follow")
                    .child(firebaseUser!!.uid).child("following").child(user.id).setValue(true)
                logEvent()


                FirebaseDatabase.getInstance().reference.child("follow")
                    .child(user.id).child("followers").child(firebaseUser!!.uid).setValue(true)
                addNotifications(user.id)
            } else
            {

                FirebaseDatabase.getInstance().reference.child("follow")
                    .child(firebaseUser!!.uid).child("following").child(user.id).removeValue()
                FirebaseDatabase.getInstance().reference.child("follow")
                    .child(user.id).child("followers").child(firebaseUser!!.uid).removeValue()

                FirebaseDatabase.getInstance().reference.child("notifications").child(user.id)
                    .child("isFollowing").removeValue()

            }
        }
    }

    class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.txt_name
        var img: CircleImageView = view.img_profile
        var follow: Button = view.btn_follow

    }


    private fun isFollowing(userId:String,button:Button) {
        val database = FirebaseDatabase.getInstance().reference
            .child("follow").child(firebaseUser!!.uid).child("following")

        database.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if(dataSnapshot.child(userId).exists()){
                    button.text = context.getString(R.string.following)
                }else{
                    button.text = context.getString(R.string.follow)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
    }

    private fun addNotifications(userId:String){
        val ref = FirebaseDatabase.getInstance().reference
            .child("notifications").child(userId)

        val addNotification = Notification(
            "","is following you",firebaseUser!!.uid,""
        )

        ref.child("isFollowing").setValue(addNotification)

    }

    private fun logEvent() {
        val bundle = Bundle()
        bundle.putString("User", firebaseUser!!.uid)
        firebaseAnalytics.logEvent("Follow", bundle)
    }
}
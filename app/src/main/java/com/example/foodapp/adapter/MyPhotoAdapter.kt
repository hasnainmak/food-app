package com.example.foodapp.adapter

import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.foodapp.R
import com.example.foodapp.utils.Constant
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import androidx.fragment.app.FragmentActivity
import com.example.foodapp.model.Post
import com.example.foodapp.PostDetailFragment
import kotlinx.android.synthetic.main.feed_list_item.view.*


class MyPhotoAdapter(var context: Context, var dataSource: MutableList<Post>) :
    RecyclerView.Adapter<MyPhotoAdapter.MyImageViewHolder>() {


    private var firebaseUser: FirebaseUser? = null
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyImageViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.profile_image_list_item, parent, false)
        return MyImageViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataSource.size
    }

    override fun onBindViewHolder(holder: MyImageViewHolder, position: Int) {
        firebaseUser = FirebaseAuth.getInstance().currentUser

        val post = dataSource[position]
        Glide.with(context).load(post.postImage).into(holder.postImg)

        holder.itemView.setOnClickListener {
            sharedPreferences =
                context.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString(Constant.KEY_POST_ID,post.postId)
            editor.apply()

            (context as FragmentActivity).supportFragmentManager.beginTransaction().addToBackStack("stack")
                .replace(R.id.fragment_container, PostDetailFragment())
                .commit()
        }


    }

    class MyImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var postImg = view.img_post!!



    }


}
package com.example.foodapp.adapter

import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.foodapp.model.User
import com.example.foodapp.R
import com.example.foodapp.utils.Constant
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import androidx.fragment.app.FragmentActivity
import com.example.foodapp.fragments.ProfileFragment
import com.example.foodapp.model.Notification
import com.example.foodapp.model.Post
import com.example.foodapp.PostDetailFragment
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.notification_list_view.view.*


class NotificationAdapter(val context: Context, val dataSource: MutableList<Notification>) :
    RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder>() {

    private var firebaseUser: FirebaseUser? = null
    private lateinit var preferences: SharedPreferences


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.notification_list_view, parent, false)

        return NotificationViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataSource.size
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        firebaseUser = FirebaseAuth.getInstance().currentUser

        val notification = dataSource[position]

        holder.txtNotificationTitle.text = notification.text

        publishedInfo(holder.userName, holder.profileImg, notification.userId)


        if (notification.validPost == "true") {
            holder.postImg.visibility = View.VISIBLE
            getPostImage(holder.postImg,notification.postId)
        }else{
            holder.postImg.visibility = View.GONE

        }

        holder.itemView.setOnClickListener {
            if (notification.validPost == "true") {
                preferences = context.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE)
                val editor = preferences.edit()
                editor.putString(Constant.KEY_POST_ID, notification.postId)
                editor.apply()
                (context as FragmentActivity).supportFragmentManager.beginTransaction().addToBackStack("stack")
                    .replace(R.id.fragment_container, PostDetailFragment())
                    .commit()
            }else {
                preferences = context.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE)
                val editor = preferences.edit()
                editor.putString(Constant.KEY_PROFILE_ID, notification.userId)
                editor.apply()
                (context as FragmentActivity).supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, ProfileFragment())
                    .commit()
            }

        }


    }

    class NotificationViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val userName: TextView = view.txt_name
        val profileImg: CircleImageView = view.img_profile
        val postImg: ImageView = view.img_post
        val txtNotificationTitle: TextView = view.txt_notificationTitle

    }


    private fun getPostImage(img_post:ImageView,postId:String){
        val reference = FirebaseDatabase.getInstance().getReference("posts").child(postId)
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                val post = dataSnapshot.getValue(Post::class.java)
                Glide.with(context).load(post?.postImage).into(img_post)
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
    }


    private fun publishedInfo(username: TextView,profileImg:ImageView, userId: String) {
        val reference = FirebaseDatabase.getInstance().getReference("users").child(userId)
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                val user = dataSnapshot.getValue(User::class.java)
                username.text = user?.name
                Glide.with(context).load(user?.profileImg).into(profileImg)
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
    }


}
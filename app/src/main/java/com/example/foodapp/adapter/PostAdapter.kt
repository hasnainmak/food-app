package com.example.foodapp.adapter

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.marginStart
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.foodapp.model.User
import com.example.foodapp.utils.Constant
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import androidx.fragment.app.FragmentActivity
import com.example.foodapp.fragments.ProfileFragment
import com.example.foodapp.model.Notification
import com.example.foodapp.model.Post
import com.example.foodapp.ReservationFragment
import kotlinx.android.synthetic.main.feed_list_item.view.*
import kotlinx.android.synthetic.main.user_item.view.img_profile
import kotlinx.android.synthetic.main.user_item.view.txt_name
import com.example.foodapp.R
import com.example.foodapp.fragments.HomeFragment
import com.example.foodapp.utils.showToast
import com.google.firebase.analytics.FirebaseAnalytics
import de.hdodenhof.circleimageview.CircleImageView


class PostAdapter(var context: Context, var dataSource: MutableList<Post>) :
    RecyclerView.Adapter<PostAdapter.PostViewHolder>() {

    private var SECOND_MILLIS = 1000
    private var MINUTE_MILLIS = 60 * SECOND_MILLIS
    private var HOUR_MILLIS = 60 * MINUTE_MILLIS
    private var DAY_MILLIS = 24 * HOUR_MILLIS
    private lateinit var preferences: SharedPreferences
    private var firebaseUser: FirebaseUser? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.feed_list_item, parent, false)

        return PostViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataSource.size
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        firebaseUser = FirebaseAuth.getInstance().currentUser


        val post = dataSource[position]
        Glide.with(context).load(post.postImage).into(holder.postImg)

        if (post.description == "") {
            holder.description.visibility = View.GONE
        }else{
            holder.description.visibility = View.VISIBLE
            holder.description.text = post.description
        }


        publishedInfo(holder.username,holder.profileImg, post.userId)
        isLiked(post.postId,holder.img_like)
        likesCount(post.postId,holder.likesCount)

        if (post.userId == firebaseUser!!.uid) {

             holder.btn_book.visibility = View.GONE
        }else{
            holder.btn_book.visibility = View.VISIBLE
            holder.btn_book.setOnClickListener {
                preferences = context.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE)
                val editor = preferences.edit()
                editor.putString(Constant.KEY_PROFILE_ID, post.userId)
                editor.apply()
                (context as FragmentActivity).supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, ReservationFragment())
                    .commit()
            }
        }



        holder.username.setOnClickListener {
            preferences = context.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE)
            val editor = preferences.edit()
            editor.putString(Constant.KEY_PROFILE_ID,post.userId)
            editor.apply()
            (context as FragmentActivity).supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, ProfileFragment())
                .commit()
        }

        if (post.userId == firebaseUser!!.uid) {
            holder.more.visibility = View.VISIBLE
        holder.more.setOnClickListener {
            val popupMenu = PopupMenu(context, holder.more)
            popupMenu.menuInflater.inflate(R.menu.post_menu,popupMenu.menu)
            popupMenu.setOnMenuItemClickListener { item ->
                when(item.itemId) {
                    R.id.edit_post -> {
                        editPost(post.postId)
                    }

                    R.id.delete_post ->{

                        AlertDialog.Builder(context)
                            .setTitle("Delete Post")
                            .setMessage("Are you sure? you want to delete this")
                            .setPositiveButton("Delete") { dialog, _ ->

                                FirebaseDatabase.getInstance().getReference("posts")
                                    .child(post.postId).removeValue()
                                    .addOnCompleteListener { task ->
                                        if (task.isSuccessful) {

                                            context.showToast("Post Deleted")
                                            dialog.dismiss()
                                        }
                                    }
                            }
                            .setNegativeButton("Cancel") { dialog, _ ->

                                dialog.dismiss()
                            }
                            .create()
                            .show()
                    }

                }
                true
            }
            popupMenu.show()

        }
    }else{
            holder.more.visibility = View.GONE
        }

        //logic for like
        holder.img_like.setOnClickListener {
            if ((holder.img_like.tag.equals("like"))) {
                FirebaseDatabase.getInstance().reference
                    .child("likes")
                    .child(post.postId).child(firebaseUser!!.uid).setValue(true)


                addNotifications(post.userId,post.postId)

            }else{
                FirebaseDatabase.getInstance().reference
                    .child("likes")
                    .child(post.postId).child(firebaseUser!!.uid).removeValue()

                FirebaseDatabase.getInstance().reference.child("notifications").child(post.userId)
                    .child("isLiked ${post.postId}").removeValue()
            }
        }

        //logic for isValidOffer

        //logic for postTime
        val time = post.currenTime.toLong()
        holder.postTime.text = getTimeAgo(time)



    }

    class PostViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var username: TextView = view.txt_name
        var profileImg: CircleImageView = view.img_profile
        var postTime: TextView = view.txt_time
        var postImg: ImageView = view.img_post
        var description: TextView = view.txt_description
        var img_like: ImageView = view.img_like
        var likesCount: TextView = view.txt_likesCount
        var btn_book: Button = view.btn_book
        var more: ImageView = view.img_more


    }

    private fun editPost(postId: String){
        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setTitle("Edit Post")
        val editText = EditText(context)
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        editText.layoutParams = lp
        alertDialog.setView(editText)
        getDescriptionText(postId,editText)

        alertDialog.setPositiveButton("Edit") { _, _ ->
                val et = editText.text.toString()
                 FirebaseDatabase.getInstance().getReference("posts")
                    .child(postId).child("description").setValue(et)
               context.showToast("Description updated")

            }
            .setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }
            .create()
            .show()

    }

    private fun getDescriptionText(postId: String,editText: EditText){

        val ref = FirebaseDatabase.getInstance().reference
            .child("posts")
            .child(postId)

        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                when {
                    dataSnapshot.exists() -> editText.setText(dataSnapshot.getValue(Post::class.java)!!.description)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })

    }


    private fun isLiked(postId:String,img_liked:ImageView){

        val ref = FirebaseDatabase.getInstance().reference
            .child("likes")
            .child(postId)

        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if(dataSnapshot.child(firebaseUser!!.uid).exists()){
                    img_liked.setImageResource(R.drawable.ic_favorite_black_24dp)
                    img_liked.tag = "liked"
                }else{
                    img_liked.setImageResource(R.drawable.ic_like)
                    img_liked.tag = "like"
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
    }

    private fun addNotifications(userId:String,postId: String){
        val ref = FirebaseDatabase.getInstance().reference
            .child("notifications").child(userId)

        val addNotification = Notification(
            postId,"Liked your post",firebaseUser!!.uid,"true"
        )

        ref.child("isLiked $postId").setValue(addNotification)

    }

    private fun likesCount(postId:String,likes:TextView){

        val ref = FirebaseDatabase.getInstance().reference
            .child("likes")
            .child(postId)

        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                likes.text = "${dataSnapshot.childrenCount} likes"

            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
    }


    private fun publishedInfo(username: TextView,profileImg:ImageView, userId: String) {
        val reference = FirebaseDatabase.getInstance().getReference("users").child(userId)
        reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                val user = dataSnapshot.getValue(User::class.java)
                username.text = user?.name
                Glide.with(context).load(user?.profileImg).into(profileImg)
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
    }

    private fun getTimeAgo(getTime: Long): String? {
        var time = getTime
        if (time < 1000000000000L) {
            time *= 1000
        }

        val now = System.currentTimeMillis()
        if (time > now || time <= 0) {
            return null
        }


        val diff = now - time
        return when {
            diff < MINUTE_MILLIS -> "just now"
            diff < 2 * MINUTE_MILLIS -> "a minute "
            diff < 50 * MINUTE_MILLIS -> "${diff / MINUTE_MILLIS} minutes "
            diff < 90 * MINUTE_MILLIS -> "an hour "
            diff < 24 * HOUR_MILLIS -> "${diff / HOUR_MILLIS} hours "
            diff < 48 * HOUR_MILLIS -> "yesterday"
            else -> "${diff / DAY_MILLIS} days"
        }
    }


}
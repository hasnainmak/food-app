package com.example.foodapp.adapter

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.foodapp.model.Reservation
import com.example.foodapp.R
import com.example.foodapp.model.Notification
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.booking_list_item.view.*


class ReservationAdapter(var context: Context, var dataSource: MutableList<Reservation>) :
    RecyclerView.Adapter<ReservationAdapter.ReservationViewHolder>() {

    private var firebaseUser: FirebaseUser? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReservationViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.booking_list_item, parent, false)
        return ReservationViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataSource.size
    }

    override fun onBindViewHolder(holder: ReservationViewHolder, position: Int) {
        firebaseUser = FirebaseAuth.getInstance().currentUser
        val reservation = dataSource[position]


        holder.name.text = reservation.name
        holder.phone.text = reservation.phone
        holder.date.text = reservation.date
        holder.noOfPerson.text = reservation.noOfPerson
        holder.time.text = reservation.time

        if(reservation.pending.equals("true")){
            holder.btnStatus.setBackgroundColor((Color.parseColor("#2E7D32")))
            holder.btnStatus.text = context.getString(R.string.approved)
        }else{
            holder.btnStatus.setBackgroundColor((Color.parseColor("#B71C1C")))
            holder.btnStatus.text = context.getString(R.string.pending)

        }

        holder.btn_delete.setOnClickListener {

            AlertDialog.Builder(context)
                .setTitle("Delete")
                .setMessage(context.getString(R.string.delete_string))
                .setPositiveButton("Delete") { dialog, _ ->


                    FirebaseDatabase.getInstance().reference.child("reservations")
                        .child(reservation.reservationId.toString())
                        .removeValue()
                    dialog.dismiss()
                    if (reservation.pending == "true") {
                        FirebaseDatabase.getInstance().reference.child("notifications")
                            .child(reservation.userId.toString())
                            .child("isBooked ${reservation.reservationId.toString()}").removeValue()
                    }


                }
                .setNegativeButton("Close") { dialog, _ ->
                    dialog.dismiss()
                }
                .create()
                .show()


        }



        if(reservation.description.equals("")){
            holder.description.visibility = View.GONE
        }else{
            holder.description.visibility = View.VISIBLE
            holder.description.text = reservation.description
        }

        if (reservation.bookTo == firebaseUser!!.uid) {
            holder.btnStatus.isEnabled = true
            holder.btnStatus.setOnClickListener {

                AlertDialog.Builder(context)
                    .setTitle("Reservation Status")
                    .setMessage(context.getString(R.string.alert_message))
                    .setPositiveButton("Approve") { _, _ ->

                        val reference = FirebaseDatabase.getInstance().getReference("reservations")
                            .child(reservation.reservationId.toString())

                        reference.child("pending").setValue("true")

                        addNotifications(
                            reservation.reservationId.toString(),
                            reservation.userId.toString()
                        )



                    }
                    .setNegativeButton("Pending") { _, _ ->

                        val reference = FirebaseDatabase.getInstance().getReference("reservations")
                            .child(reservation.reservationId.toString())

                        reference.child("pending").setValue("false")
                        FirebaseDatabase.getInstance().reference.child("notifications")
                            .child(reservation.userId.toString())
                            .child("isBooked ${reservation.reservationId.toString()}").removeValue()
                    }
                    .create()
                    .show()

            }
        } else {
            holder.btnStatus.isEnabled = false
        }

    }

    class ReservationViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val name: TextView = view.txt_name
        val time: TextView = view.txt_time
        val description: TextView = view.txt_description
        val phone: TextView = view.txt_phone
        val noOfPerson: TextView = view.txt_noOfPerson
        val date: TextView = view.txt_date
        val btnStatus: Button = view.btn_status
        var btn_delete: ImageView = view.img_delete

    }

    private fun addNotifications(reservationId: String, userId: String) {
        val ref = FirebaseDatabase.getInstance().reference
            .child("notifications").child(userId)

        val addNotification = Notification(
            "", "Approved your reservation", firebaseUser!!.uid, ""
        )

        ref.child("isBooked $reservationId").setValue(addNotification)

    }


}
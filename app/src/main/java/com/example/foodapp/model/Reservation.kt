package com.example.foodapp.model

data class Reservation (
    var reservationId:String? = "",
    var userId:String?= "",
    var bookTo:String? = "",
    var name:String?= "",
    var phone:String?= "",
    var noOfPerson:String? = "",
    var date:String? = "",
    var time:String?= "",
    var pending:String? = "",
    var description:String? = ""

)
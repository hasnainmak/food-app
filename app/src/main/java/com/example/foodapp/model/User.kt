package com.example.foodapp.model

data class User (

    var id:String="",
    var name:String = "",
    var email:String = "",
    var password:String = "",
    var address:String? = "",
    var phone:String? = "",
    var shopTiming:String? = "",
    var profileImg:String? = ""

)
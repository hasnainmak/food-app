package com.example.foodapp.model

data class Notification (
    var postId:String = "",
    var text:String = "",
    var userId:String = "",
    var validPost:String = ""

)
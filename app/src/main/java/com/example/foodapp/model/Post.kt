package com.example.foodapp.model

data class Post (
    var postId:String = "",
    var userId:String = "",
    var postImage:String = "",
    var description: String? = "",
    var currenTime:String = ""
)